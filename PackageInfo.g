############################################################################
##
##  PackageInfo.g
##  Copyright (C) 2020                            João Araújo & Choiwah Chow
##
##  Licensing information can be found in the README file of this package.
##
#############################################################################
##

##  <#GAPDoc Label="PKGVERSIONDATA">
##  <!ENTITY VERSION "1.0.1">
##  <!ENTITY COPYRIGHTYEARS "2020">
##  <!ENTITY ARCHIVENAME "magmaut-0.1.0">
##  <#/GAPDoc>

SetPackageInfo( rec(
PackageName := "magmaut",
Subtitle := "Computing automorphisms and isomorphisms of magmas in GAP",
Version := "0.0.1",
Date := "04/07/2020",

Persons := [
  rec(
    LastName      := "Araújo",
    FirstNames    := "João",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "jjrsga@gmail.com",
    WWWHome       := "https://docentes.fct.unl.pt/jj-araujo/"
  ),
  rec(
    LastName      := "Janota",
    FirstNames    := "Mikoláš",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "mikolas.janota@gmail.com",
    WWWHome       := "http://sat.inesc-id.pt/~mikolas/"
  ),
  rec(
    LastName      := "Chow",
    FirstNames    := "Choiwah",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "choiwah.chow@gmail.com",
    WWWHome       := "https://orcid.org/0000-0002-8383-8057"
  ),
],

Status := "dev",
CommunicatedBy := "Choiwah Chow",
AcceptDate := "N/A",

PackageWWWHome  := "https://gap-packages.github.io/magmaut/",
README_URL      := Concatenation( ~.PackageWWWHome, "README" ),
PackageInfoURL  := Concatenation( ~.PackageWWWHome, "PackageInfo.g" ),
SourceRepository := rec(
    Type := "git",
    URL := "https://github.com/gap-packages/magmaut/",
),
IssueTrackerURL := Concatenation( ~.SourceRepository.URL, "/issues" ),
ArchiveURL      := Concatenation( ~.SourceRepository.URL,
                                 "/releases/download/v", ~.Version,
                                 "/magmaut-", ~.Version ),
ArchiveFormats := ".tar.gz",

AbstractHTML := Concatenation(
"The MAGMAUT package provides researchers in computational algebra ",
"with a computational tool to compute the automorphism and the ",
"isomorphims of magmas in the computational algebra system GAP."
),

PackageDoc := rec(
  BookName  := "magma",
  ArchiveURLSubset := ["doc"],
  HTMLStart := "doc/chap0_mj.html",
  PDFFile   := "doc/manual.pdf",
  SixFile   := "doc/manual.six",
  LongTitle := "The MAGMAUT Package: Automorphism and Isomorphism of Magmas for GAP",
  Autoload  := true     # only for the documentation, TEMPORARILY TURNED OFF
),

Dependencies := rec(
  GAP := ">=4.10",
  NeededOtherPackages := [],
  SuggestedOtherPackages := [],
  ExternalConditions := []
),

AvailabilityTest := ReturnTrue,
TestFile := "tst/testall.g",
Keywords := ["magma", "automorphism of magma", "magmaut_automorphism", 
			 "isomorphism of magma"],

AutoDoc := rec(
    TitlePage := rec(
        Title := "The MAGMAUT Package",
        Abstract := ~.AbstractHTML,
        Copyright := """
<Index>License</Index>
&copyright; 2020-2023 João Araújo, Mikoláš Janota, and Choiwah Chow.<P/>
The &MAGMAUT; package is free software;
you can redistribute it and/or modify it under the terms of the
<URL Text="GNU General Public License">https://www.fsf.org/licenses/gpl.html</URL>
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
""",
        Acknowledgements := """
""",
    ),
),

));

#############################################################################

##
#W  read.g                  Magmaut                               João Araújo
#W                                                               Choiwah Chow
##
##
##

#############################################################################
##
#R  Read the install files.
##  -------------------------------------------------------------------------
LoadDynamicModule(Filename(DirectoriesPackagePrograms("magmaut"),"iso.la.so"));
ReadPackage("magmaut", "lib/magmaut.gi");   # automorphisms and isomorphisms

#E  read.g . . . . . . . . . . . . . . . . . . . . . . . . . . . .  ends here

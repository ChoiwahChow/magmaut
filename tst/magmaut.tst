#############################################################################
##
#W  magmaut.tst   Testing isomorphisms/automorphisms
##
##
##
gap> START_TEST("MAGMAS, magmaut: testing auto and iso");

# TESTING MAGA_Invariants
gap> Length(MAGMAS_Invariants(AllSmallGroups(16)[1]));
2
gap> a := Group((1,2), (3,4));;
gap> a = AllSmallGroups(4)[2];
false
gap> S := MAGMAS_Invariants(a);;
gap> T := MAGMAS_Invariants(AllSmallGroups(4)[2]);;
gap> AreEqualMAGMA_Invariants(S, T);
true

# TESTING AUTOMORPHISM GROUPS
gap> Magmaut_AutomorphismGroup(AllSmallGroups(16)[1]);
<permutation group with 7 generators>
gap> Sum(List(List(AllSmallGroups(8), G->Magmaut_AutomorphismGroup(G)), G->Size(G)));
212
gap> Magmaut_AutomorphismGroup(MagmaByMultiplicationTable([[1,2],[1,2]]));
Group([ (1,2) ])
gap> a := Magmaut_AutomorphismGroup(AllSmallGroups(8)[2]);;
gap> b := AutomorphismGroup(MagmaByMultiplicationTable(MultiplicationTable(AllSmallGroups(8)[2])));;
gap> a = b;
true
gap> a := AllSmallGroups(4);;
gap> b := List(a, t->MultiplicationTable(t));;   # algebra with 2 binary operations
gap> Add(b, [3, 2, 1, 4]);;    # add unary operation
gap> Algebra_AutomorphismGroup(b);
Group([ (2,4) ])

# TESTING ISOMORPHISMS
gap> IsomorphismMagmas(AllSmallGroups(32)[1], AllSmallGroups(32)[2]);
fail
gap> IsomorphismMagmas(AllSmallGroups(32)[1], AllSmallGroups(32)[1]);
()
gap> m1 := MagmaByMultiplicationTable([[1,2,3],[1,2,3],[3,1,3]]);
<magma with 3 generators>
gap> m2 := MagmaByMultiplicationTable([[1,2,3],[1,2,3],[2,3,3]]);
<magma with 3 generators>
gap> IsomorphismMagmas(m1, m2);
(1,2)
gap> 

#
gap> STOP_TEST( "magmaut.tst", 1000000 );


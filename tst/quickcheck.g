
LoadPackage("magmaut");;
LoadPackage("semigroup");;
LoadPackage("semigroups");;
LoadPackage("smallsemi");;
LoadPackage("loops");;


test_magma_auto_g := function(s)
	local  a, b, c, x;
	a := AllSmallGroups(s);
	for x in [1..Length(a)] do
		b := MAGMAS_AutomorphismGroup(a[x], []);
		c := AutomorphismGroup(a[x]);
		if IsomorphismGroups(b, c) = fail then
			Print("Test failed for Group # "); Print(x); Print("\n");
		fi;
	od;
end;

test_magma_auto_l := function(s, e)
	local  a, b, c, x;
	for x in [s..e] do
		a := RandomLoop( x );
		b := MAGMAS_AutomorphismGroup(a, []);
		c := AutomorphismGroup(a);
		if IsomorphismGroups(b, c) = fail then
			Print("Test failed for Loop # "); Print(x); Print("\n");
		fi;
	od;
end;

test_magma_auto_q := function(s, e)
	local  a, b, c, x;
	for x in [s..e] do
		a := RandomQuasigroup( x );
		b := MAGMAS_AutomorphismGroup(a, []);
		c := AutomorphismGroup(a);
		if IsomorphismGroups(b, c) = fail then
			Print("Test failed for Quasigroup # "); Print(x); Print("\n");
		fi;
	od;
end;


test_magma_auto_s := function()
	local  a, b, c, x;
	a := List([1..1160], G->SmallSemigroup(5, G));
	for x in [1..Length(a)] do
		b := MAGMAS_AutomorphismGroup(a[x], []);
		c := AutomorphismGroup(a[x]);
		if IsomorphismGroups(b, c) = fail then
			Print("Test failed for Group # "); Print(x); Print("\n");
		fi;
	od;
end;



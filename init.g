#############################################################################
##
#W    init.g                  The Magmaut Package                 João Araújo
#W                                                               Choiwah Chow
##

#############################################################################
##
#R  Read the declaration files.
##
ReadPackage( "magmaut", "lib/magmaut.gd");   # automorphisms and isomorphisms

#E  init.g . . . . . . . . . . . . . . . . . . . . . . . . . . . .  ends here

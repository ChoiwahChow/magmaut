/****************************************************************************

This files follows the example given in the Example-4.2.1 package.

The following documentation is from the hellod.c file in the src directory of the Example package.

This is a prototype of a HELLO_WORLD example implemented as a kernel module.

An example of its compilation:

dyn-194-155:src alexk$ ~/gap4r4/bin/i686-apple-darwin10.4.0-gcc/gac -d hellod.c
gcc -fPIC -o /var/folders/NB/NBxYeZaRHxGpf4haob7SqE+++TQ/-Tmp-//gac36146/36146_hellod.o -I/Users/alexk/gap4r4/bin/i686-apple-darwin10.4.0-gcc/../.. -I/Users/alexk/gap4r4/bin/i686-apple-darwin10.4.0-gcc -DCONFIG_H -c hellod.c
ld -bundle -bundle_loader /Users/alexk/gap4r4/bin/i686-apple-darwin10.4.0-gcc/gap /usr/lib/bundle1.o -lc -lm -o hellod.so /var/folders/NB/NBxYeZaRHxGpf4haob7SqE+++TQ/-Tmp-//gac36146/36146_hellod.o
rm -f /var/folders/NB/NBxYeZaRHxGpf4haob7SqE+++TQ/-Tmp-//gac36146/36146_hellod.o

It works as follows:

gap> LoadDynamicModule("hellod.so");
gap> HELLO_WORLD();
Hello World!
gap>

It may be included in one of the future versions of the Example package.
However, this will require documenting more rules of kernel programming.

****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "src/compiled.h"


static void c_copy_partial_map(int map_size, int map[], int domain[], int range[],
		int map2[], int domain2[], int range2[]);
static int c_extend_homomorphism_by_closing_source(int c_map[], int c_domain[], int c_range[], int domain_size,
		int s, int c_L[][s+1], int m, int c_M[][m+1]);
static int c_extend_isomorphism(int c_map[], int c_domain[], int c_range[], int domain_size,
								int s, int c_L[][s+1], int c_GenL[], int d_size, int c_DisL[][domain_size+1],
								int m, int c_M[][m+1], int c_DisM[][domain_size+1]);
static int c_extend_map(int x, int y, int z, int fz, int c_map[],
		int c_domain[], int c_range[], int c_newNow[], int has_new);
static int find_num_unique_el(int s, int vec[]);
static int c_find_vec_head(int s, int *vec);
static int count_non_zero(int s, int vec[]);
static int c_find_possible_range(int row_size, int domain_size, int x, int c_DisL[][domain_size+1],
		int c_DisM[][domain_size+1], int c_range[domain_size+1], int possible_images[domain_size+1]);
static void c_invariant_vec(int domain_size, int c_MT[][domain_size+1],
		int c_invariant_size, int c_vec[][c_invariant_size+1], int struct_type);

static int c_length_vec(int s, int vec[s+1]);
static void c_zero_vec(int s, int* vec);

static void gap_list_to_c_intarr(int s, int* arr, Obj f);
static void gap_list2_to_c_int2d(int s, int len, int arr[][s+1], Obj f);
static void gap_to_c_int2d(int s, int arr[][s], Obj f);
static void gap_to_c_intarr(int[], Obj);
static int is_latin_square(int domain_size, int c_MT[][domain_size+1]);
static void set_invariant_subset(int c_opt[], int opt_type[], int size_opt_type);



static void gap_to_c_intarr(int arr[], Obj f)
{
	for(int idx = 1; idx <= LEN_PLIST(f); ++idx)
		arr[idx] = (int)INT_INTOBJ(ELM_PLIST(f, idx));
}

static void gap_to_c_int2d(int s, int arr[][s], Obj f)
{
	for(int idx = 1; idx <= LEN_PLIST(f); ++idx) {
		register Obj row = ELM_PLIST(f, idx);
		for(int jdx = 1; jdx <= LEN_PLIST(row); ++jdx)
			arr[idx][jdx] = (int)INT_INTOBJ(ELM_PLIST(row, jdx));
	}
}


static void gap_list_to_c_intarr(int s, int* arr, Obj f)
{
	/* GAP list to 1d int array
	 * The row will be filled with zeros at the end to make up the length s+1
	 * The first zero marks the end of the array
	 */
	int f_len = LEN_PLIST(f);
	for(int idx = 1; idx <= f_len; ++idx)
		*arr++ = (int)INT_INTOBJ(ELM_PLIST(f, idx));
	for (int idx = f_len+1; idx <= s; ++idx)
		*arr++ = 0;
}


static void gap_list2_to_c_int2d(int s, int len, int arr[][len+1], Obj f)
{
	/* GAP list of list to 2d int array, which is not necessarily a square matrix
	 * Each row will be filled with ending zero at the end if there are empty slots
	 */
	for(int idx = 1; idx <= s; ++idx) {
		register Obj row = ELM_PLIST(f, idx);
		for (int jdx = 1; jdx <= LEN_PLIST(row); ++jdx) {
			arr[idx][jdx] = (int)INT_INTOBJ(ELM_PLIST(row, jdx));
		}
		if (LEN_PLIST(row) < len)
			arr[idx][LEN_PLIST(row)+1] = 0;
	}
}


static int c_length_vec(int s, int vec[])
{
	/* vec is an int array with zero signifies empty slot, and 1 means present. So
	 * sum up the values (1 or 0) in all the slot, and that would be the length of vec.
	 */
	int length = 0;
	for (int idx = 1; idx <=s; ++idx)
		length += vec[idx];
	return length;
}


static int c_find_vec_head(int s, int* vec)
{
	/* vec is an int array with zero signifies empty slot.  So find the first slot
	 * that is not empty, and return the index.
	 */
	for (int idx = 1; idx <= s; ++idx) {
		if (*vec++ > 0) {
			return idx;
		}
	}
	return -1;   // should not happen
}


static int c_extend_map(int x, int y, int z, int fz, int c_map[],
		int c_domain[], int c_range[], int c_newNow[], int has_new)
{
	if (c_map[z] == 0) {  // found new element that has not been mapped
		c_map[z] = fz;
		c_domain[z] = 1;
		if (c_range[fz] == 0) {
			c_range[fz] = 1;
		}
		c_newNow[z] = 1;
		has_new = 1;
	}
	else if (c_map[z] != fz) // check homomorphism
		return -1;
	return has_new;
}


int c_extend_homomorphism_by_closing_source(int c_map[], int c_domain[], int c_range[], int domain_size,
		int s, int c_L[][s+1], int m, int c_M[][m+1]) {
	/* c_map - partial mapping
	 * c_domain - the partial domain so far - c_domain[x] = 1 means x is in the domain
	 * c_range - the partial range so far - c_range[x] = 1 means x is in the range
	 * domain_size - domain size
	 * c_L - multiplication table of size s for the domain magma
	 * c_M - multiplication table of size s for the range magma
	 *
	 * Returns 1 if new mappings done, 0 if not, and -1 if contradiction detected.
	 */

	// inputs

	int c_oldS[domain_size+1];   // domain elements in this list has been multiplied pairwise for closure
	int c_newS[domain_size+1];	 // domain elements in this list has not been multiplied pairwise for closure
	for (int idx = 1; idx <= domain_size; ++idx) {
		c_oldS[idx] = 0;
		c_newS[idx] = c_domain[idx];
	}

	int has_new = 1;
	while (has_new > 0)
	{
		has_new = 0;
		int c_newNow[domain_size+1];  // c_newNow[z] == 1 means z is in the set c_newNow
		for (int idx = 1; idx <= domain_size; ++idx)
			c_newNow[idx] = 0;

		for (int x = 1; x <= domain_size; ++x) {
			if (c_oldS[x] > 0) {
				for (int y = 1; y <= domain_size; ++y)
					if (c_newS[y] > 0) {
						has_new = c_extend_map(x, y, c_L[x][y], c_M[c_map[x]][c_map[y]],
								c_map, c_domain, c_range, c_newNow, has_new);
						if (has_new == -1)
							return -1;
						has_new = c_extend_map(y, x, c_L[y][x], c_M[c_map[y]][c_map[x]],
								c_map, c_domain, c_range, c_newNow, has_new);
						if (has_new == -1)
							return -1;
					}
			}
		}
		for (int x = 1; x <= domain_size; ++x) {
			if (c_newS[x] > 0){
				for (int y = 1; y <= domain_size; ++y) {
					if (c_newS[y] > 0) {
						has_new = c_extend_map(x, y, c_L[x][y], c_M[c_map[x]][c_map[y]],
								c_map, c_domain, c_range, c_newNow, has_new);
						if (has_new == -1)
							return -1;
					}
				}
			}
		}
		if (has_new > 0) {
			for (int idx = 1; idx <= domain_size; ++idx) {
				if (c_newS[idx] > 0)
					c_oldS[idx] = c_newS[idx];
				c_newS[idx] = c_newNow[idx];
			}
		}
	}
	return has_new;
}


static int c_find_possible_range(int row_size, int domain_size, int x, int c_DisL[][domain_size+1],
		int c_DisM[][domain_size+1], int c_range[domain_size+1], int possible_images[domain_size+1])
{
	/*
	 * DisL/M is a 2D int array of row_size+1 x domain_size+1, pos 0 is unused
	 * e.g. [[4], [2,3]] would be represented by
	 * DisL[1][1] = 4, DisL[1][2] = 0 (and the rest of the array zeros)
	 * DisL[2][1] = 2, DisL[2] = 3, DisL[3] = 0 (the rest of the array zeros).
	 *
	 * x is the element in the domain to look for
	 * c_range[y] = 1 means y is already used, 0 means note
	 * possible_image is an int array for the possible mapping of x to return. The array
	 * is assumed to be filled with zeros before coming to this function.
	 */
	/* Find x in DisL */
	for (int idx = 1; idx <= row_size; ++idx) {
		for (int jdx = 1; jdx <= domain_size && c_DisL[idx][jdx] > 0; ++jdx) {
			if (x == c_DisL[idx][jdx]) {
				int ptr = 1;
				for (int kdx = 1; kdx <= domain_size && c_DisM[idx][kdx] > 0; ++kdx) {
					if (c_range[c_DisM[idx][kdx]] == 0)	{
						possible_images[ptr] = c_DisM[idx][kdx];
						++ptr;
					}
				}
				return 1;
			}
		}
	}
	return 0;
}


static void c_copy_partial_map(int map_size, int map[], int domain[], int range[],
		int map2[], int domain2[], int range2[])
{
	for (int idx = 1; idx <= map_size; ++idx) {
		map[idx] = map2[idx];
		domain[idx] = domain2[idx];
		range[idx] = range2[idx];
	}
}


static void c_zero_vec(int s, int* vec)
{
	for (int idx = 1; idx <= s; ++idx)
		*vec++ = 0;
}


static int c_extend_isomorphism(int c_map[], int c_domain[], int c_range[], int domain_size,
								int s, int c_L[][s+1], int c_GenL[], int d_size, int c_DisL[][domain_size+1],
								int m, int c_M[][m+1], int c_DisM[][domain_size+1])
{
	int has_new = c_extend_homomorphism_by_closing_source(c_map, c_domain, c_range, domain_size,
			s, c_L, m, c_M);
	if (has_new == -1)
		return -1;
	int length_domain = c_length_vec(domain_size, c_domain);
	if (length_domain > c_length_vec(domain_size, c_range))
		return -1;
	else if (length_domain == domain_size)
		return 1;   // completed, found isomorphism

	int pos = c_find_vec_head(domain_size, &c_GenL[1]);
	int x = c_GenL[pos];
	int possible_images[domain_size+1];
	c_zero_vec(domain_size, &possible_images[1]);
	c_find_possible_range(d_size, domain_size, x, c_DisL, c_DisM, c_range, possible_images);

	int s_c_map[domain_size+1];
	int s_c_domain[domain_size+1];
	int s_c_range[domain_size+1];
	c_copy_partial_map(domain_size, s_c_map, s_c_domain, s_c_range, c_map, c_domain, c_range);
	for (int ydx = 1; ydx <= domain_size && possible_images[ydx] > 0; ++ydx )
	{
		c_copy_partial_map(domain_size, c_map, c_domain, c_range, s_c_map, s_c_domain, s_c_range);
		int y = possible_images[ydx];
		c_map[x] = y;
		c_domain[x] = 1;
		c_range[y] = 1;
		c_GenL[pos] = 0;
		int g = c_extend_isomorphism(c_map, c_domain, c_range, domain_size, s, c_L, c_GenL,
				d_size, c_DisL, m, c_M, c_DisL);
		c_GenL[pos] = x;
		if (g != -1)
			return 1;
	}
	return -1;   // for failure
}


Obj MAGMAS_AutomorphismsFixingSet_C(Obj self, Obj S, Obj MT_Q, Obj x_obj, Obj GenQ, Obj DisQ)
{
	/* A - the result set - i.e. add mappings to it
	 * S - subset of domain of Q that is fixed,
	 * MT_Q - multiplicationTable of the Magma
	 * GenQ - generating set - a list of int, at most domain_size items in it
	 * DisQ - Discriminator[2] - a list of lists. e.g. [[2], [3,5], [4,6,7,8]]
	 */

	register int domain_size = LEN_PLIST(MT_Q);

	int c_S[domain_size+1];
	gap_list_to_c_intarr(domain_size, &c_S[1], S);

	int c_L[domain_size+1][domain_size+1];
	gap_list2_to_c_int2d(domain_size, domain_size, c_L, MT_Q);


	int c_genQ[domain_size+1];
	gap_list_to_c_intarr(domain_size, &c_genQ[1], GenQ);

	register int d_size = LEN_PLIST(DisQ);
	int c_DisQ[d_size+1][domain_size+1];   // C list of lists in Dis[2] - for efficient access
	gap_list2_to_c_int2d(d_size, domain_size, c_DisQ, DisQ);

	int x = (int)INT_INTOBJ(x_obj);

	int c_map[domain_size+1];
	int c_domain[domain_size+1];
	int c_range[domain_size+1];
	c_zero_vec(domain_size, &c_range[1]);
	c_range[x] = 1; // exclude x in possible_images

	int possible_images[domain_size+1];
	c_zero_vec(domain_size, &possible_images[1]);
	c_find_possible_range(d_size, domain_size, x, c_DisQ, c_DisQ, c_range, possible_images);

	Obj A = NEW_PLIST(T_PLIST, 0);
	for (int ydx = 1; ydx <= domain_size && possible_images[ydx] > 0; ++ydx )
	{
		c_zero_vec(domain_size, &c_domain[1]);
		c_zero_vec(domain_size, &c_range[1]);
		c_zero_vec(domain_size, &c_map[1]);
		int y = possible_images[ydx];
		for (int idx = 1; idx <= domain_size && c_S[idx] > 0; ++idx) { // c_S is the fixing set
			int fixed = c_S[idx];
			c_map[fixed] = fixed;
			c_domain[fixed] = 1;
			c_range[fixed] = 1;
		}
		c_map[x] = y;
		c_domain[x] = 1;
		c_range[y] = 1;
		int g = c_extend_isomorphism(c_map, c_domain, c_range, domain_size, domain_size, c_L, c_genQ,
				d_size, c_DisQ, domain_size, c_L, c_DisQ);
		if (g != -1) {
			Obj new_map = NEW_PLIST(T_PLIST, domain_size);
			for (int idx = 1; idx <= domain_size; ++idx)
				SET_ELM_PLIST(new_map, idx, INTOBJ_INT(c_map[idx]));
			SET_LEN_PLIST(new_map, domain_size);
			AddPlist(A, new_map);
		}
	}
	return A;
}


static int find_num_unique_el(int s, int vec[])
{
	/* find number of unique elements in a vector/array */
	int buf[s+1];
	c_zero_vec(s, &buf[1]);
	int sum = 0;
	for (int idx = 1; idx <= s; ++idx)
		buf[vec[idx]] = 1;
	for (int idx = 1; idx <= s; ++idx)
		if (buf[idx] > 0)
			++sum;
	return sum;
}


static int count_non_zero(int s, int vec[])
{
	/* find number of non zero elements in a vector */
	int sum = 0;
	for (int idx = 1; idx <= s; idx++)
		if (vec[idx] > 0)
			++sum;
	return sum;
}


static int is_latin_square(int domain_size, int c_MT[][domain_size+1])
{
	/* determine whether the 2-d array is a latin square */
	int c_row[domain_size];
	int c_col[domain_size];
	for (int idx = 1; idx <= domain_size; ++idx) {
		c_zero_vec(domain_size, &c_row[1]);
		c_zero_vec(domain_size, &c_col[1]);
		for (int jdx = 1; jdx <= domain_size; ++jdx) {
			if (c_row[c_MT[idx][jdx]] > 0)   // checking row idx
				return 0;
			if (c_col[c_MT[jdx][idx]] > 0)   // checking column idx
				return 0;
			c_row[c_MT[idx][jdx]] = 1;
			c_col[c_MT[jdx][idx]] = 1;
		}
	}
	return 1;
}

static int INVARIANT_SIZE = 17+1; // one extra for struct_type
static int c_group_opt[4] = {1, 12, 13, 15};
// static int c_semigroup_opt[13] = {1, 4, 5, 6, 7, 8, 9, 11, 12, 13, 15, 16, 17};
static int c_quasigroup_opt[4] = {1, 12, 14, 17};


static void set_invariant_subset(int c_opt[], int opt_type[], int size_opt_type)
{
	for (int idx = 0; idx < size_opt_type; ++idx)
		c_opt[opt_type[idx]] = opt_type[idx];
}


static void c_invariant_vec(int domain_size, int c_MT[][domain_size+1],
		int c_invariant_size, int c_vec[][c_invariant_size+1], int struct_type)
{
	/* the last element of c_vec will be the struct_type:
	 * 1 for group, 2 for semigroup, 3 for quasigroup, 4 for everything else
	 */
	int c_opt[c_invariant_size+1];
	c_zero_vec(c_invariant_size, &c_opt[1]);

	if (struct_type == 1)
		set_invariant_subset(c_opt, c_group_opt, sizeof(c_group_opt)/sizeof(c_group_opt[0]));
	else if (is_latin_square(domain_size, c_MT) == 1) {
		set_invariant_subset(c_opt, c_quasigroup_opt, sizeof(c_quasigroup_opt)/sizeof(c_quasigroup_opt[0]));
		struct_type = 3;
	}
	else {
		for (int idx = 1; idx < c_invariant_size; ++idx)
			c_opt[idx] = idx;
		c_opt[16] = 0;
		struct_type = 4;
	}

	int c_el2[domain_size+1];
	for (int idx = 1; idx <= domain_size; ++idx) {
		c_zero_vec(c_invariant_size, &c_vec[idx][1]);
		c_vec[idx][c_invariant_size] = struct_type;
		c_el2[idx] = c_MT[idx][idx];
	}

	int i_no = 1;
	/* Invariant: min exponent
	 * what is the smallest exponent that recurs, that is, s^n = s^k such that n > k > 1.
	 * n is clearly <= domain size + 1
	 * Note that s^n = (..(s*s)*s)*s)... for n of the s.
     */
	if (c_opt[i_no] > 0)
	{
		for (int el = 1; el <= domain_size; ++el) {
			int new_power = el;
			int ElPowers[domain_size+2];
			c_zero_vec(domain_size+1, &ElPowers[1]);
			ElPowers[el] = 1;
			for (int pow = 2; pow <= domain_size+2; ++pow) {
				new_power = c_MT[new_power][el];
				if (ElPowers[new_power] == 1) {
					c_vec[el][i_no] = pow;
					break;
				}
				ElPowers[new_power] = 1;
			}
		}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: Left Identity
		 * Number of elements I am a left identity
		 */
		for (int el = 1; el <= domain_size; ++el) {
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[el][jdx] == jdx)
					++(c_vec[el][i_no]);
		}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: Right Identity
		 * Number of elements I am a right identity
		 */
		for (int el = 1; el <= domain_size; ++el) {
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[jdx][el] == jdx)
					++(c_vec[el][i_no]);
		}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: number of inverses
		 * For each element x, number of elements y such that x = (xy)x
		 */
		for (int el = 1; el <= domain_size; ++el) {
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[c_MT[el][jdx]][el] == el)
					++(c_vec[el][i_no]);
		}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: distinct row elements
		 * Number of distinct elements on a row
		 */
		for (int el = 1; el <= domain_size; ++el)
			c_vec[el][i_no] = find_num_unique_el(domain_size, c_MT[el]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: distinct column elements
		 * Number of distinct elements on a column
		 */
		int column[domain_size+1];
		for (int el = 1; el <= domain_size; ++el) {
			for (int row = 1; row <= domain_size; ++row)
				column[row] = c_MT[row][el];
			c_vec[el][i_no] = find_num_unique_el(domain_size, column);
		}
	}

	++i_no;
	int idemp_no = i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: idempotent
		 * Am I idempotent? 1 or 0
		 */
		for (int el = 1; el <= domain_size; ++el)
			if (c_el2[el] == el)
				c_vec[el][i_no] = 1;
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: # of idempotents in a column
		 * how many in a column is an idempotent i.e. for a fixed a, how many m such that
		 * ma is idempotent
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_vec[c_MT[jdx][el]][idemp_no] == 1)
					++(c_vec[el][i_no]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: # of idempotents in a row
		 * how many in a row is an idempotent i.e. for a fixed a, how many m such that
		 * am is idempotent
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_vec[c_MT[el][jdx]][idemp_no] == 1)
					++(c_vec[el][i_no]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: self associative
		 * Am I self associative: x(xx) = (xx)x? 1 or 0
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[el][c_el2[jdx]] == c_MT[c_el2[jdx]][el])
					c_vec[el][i_no] = 1;
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: centralizer
		 * How many elements do I commute with
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[el][jdx] == c_MT[jdx][el])
					++(c_vec[el][i_no]);
	}


	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: double centralizer
		 * How many elements for which the square I commute with?
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[el][c_el2[jdx]] == c_MT[c_el2[jdx]][el])
					++(c_vec[el][i_no]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: square root
		 * number of element of which I am a square root
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_el2[jdx] == el)
					++(c_vec[el][i_no]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: associatizer
		 * Number of elements y satisfying x(xy) = (xx)y
		 */
		for (int el = 1; el <= domain_size; ++el)
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				if (c_MT[el][c_MT[el][jdx]] == c_MT[c_el2[el]][jdx])
					++(c_vec[el][i_no]);
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: product of commutors
		 * Number of products xy = yx = s
		 */
		for (int idx = 1; idx <= domain_size; ++idx)
			for (int jdx = 1; jdx <= domain_size; ++jdx) {
				int s = c_MT[idx][jdx];
				if (s == c_MT[jdx][idx])
					c_vec[s][i_no] += 1;
			}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: product of idempotents
		 * Number of t such that two idempotents e,f in M and s=et=tf
		 */
		int c_et[domain_size+1][domain_size+1];
		int c_tf[domain_size+1][domain_size+1];
		for (int idx = 1; idx <= domain_size; ++idx) {
			c_zero_vec(domain_size, &c_et[idx][1]);
			c_zero_vec(domain_size, &c_tf[idx][1]);
		}
		for (int el = 1; el <= domain_size; ++el) {
			if (c_el2[el] == el) {
				for (int tdx = 1; tdx <= domain_size; ++tdx) {
					c_et[tdx][el] = c_MT[el][tdx];  // all et where e is idempotent
					c_tf[tdx][el] = c_MT[tdx][el];  // all tf where f is idempotent
				}
			}
		}

		for (int tdx = 1; tdx <= domain_size; ++tdx)
			for (int jdx = 1; jdx <= domain_size; ++jdx) {
				int s = c_et[tdx][jdx];
				if (s > 0)
					for (int fdx = 1; fdx <= domain_size; ++fdx) {
						if (s == c_tf[tdx][fdx])
							c_vec[s][i_no] += 1;
					}
			}
	}

	++i_no;
	if (c_opt[i_no] > 0)
	{
		/* Invariant: ordering
		 * Number of t such that exist two elements x,y in M such that s=xy and t=yx
		 */

		// c_vec_list[s][t] == 1 means exists x, y such that s=xy and t=yx
		int c_vec_list[domain_size+1][domain_size+1];
		for (int idx = 1; idx <= domain_size; ++idx)
			c_zero_vec(domain_size, &c_vec_list[idx][1]);

		for (int idx = 1; idx <= domain_size; ++idx)
			for (int jdx = 1; jdx <= domain_size; ++jdx) {
				c_vec_list[c_MT[idx][jdx]][c_MT[jdx][idx]] = 1;
			}
		for (int el = 1; el <= domain_size; ++el)
			c_vec[el][i_no] = count_non_zero(domain_size, c_vec_list[el]);
	}
}


Obj MAGMAS_Invariants_C(Obj self, Obj MT_Q, Obj StructType)
{
	int domain_size = LEN_PLIST(MT_Q);

	int c_MT[domain_size+1][domain_size+1];
	gap_list2_to_c_int2d(domain_size, domain_size, c_MT, MT_Q);

	int c_invariant_size = INVARIANT_SIZE;
	int c_vec[domain_size+1][c_invariant_size+1];
	c_invariant_vec(domain_size, c_MT, c_invariant_size, c_vec, (int)INT_INTOBJ(StructType));

	Obj Vec = NEW_PLIST(T_PLIST, 0);
	for (int idx = 1; idx <= domain_size; ++idx) {
		Obj el_vec = NEW_PLIST(T_PLIST, c_invariant_size);
		for (int jdx = 1; jdx <= c_invariant_size; ++jdx)
			SET_ELM_PLIST(el_vec, jdx, INTOBJ_INT(c_vec[idx][jdx]));
		SET_LEN_PLIST(el_vec, c_invariant_size);
		AddPlist(Vec, el_vec);
	}
	return Vec;
}


Obj MAGMAS_ExtendHomomorphismByClosingSource_C( Obj self, Obj f, Obj L, Obj M ) {
	/* f is a List of 3 items
	 * 	f[1] is a dense list representing the map, f[1][x] = y means x is mapped to y
	 * 	f[2] is a list, the partial domain so far
	 * 	f[3] is a list, the partial range so far
	 * L is a list of lists - multiplication table for the domain magma
	 * M is a list of lists - multiplication table for the range magma
	 *
	 * Returns f, with updated f[1], and possibly new lists for f[2] and f[3]
	 */

	// inputs
	Obj in_map = ELM_PLIST(f, 1);
	int domain_size = LEN_PLIST(in_map);
	int c_map[domain_size+1];   // C partial map - for efficient access
	gap_to_c_intarr(c_map, in_map);

	register int s = LEN_PLIST(L)+1;
	int c_L[s][s];   // C multiplication table for L - for efficient access
	gap_to_c_int2d(s, c_L, L);

	register int m = LEN_PLIST(M)+1;
	int c_M[m][m];   // C multiplication table for M - for efficient access
	gap_to_c_int2d(m, c_M, M);

	int c_oldS[domain_size+1];  // elements in this list has been multiplied pairwise for closure
	int c_newS[domain_size+1];	// elements in this list has not been multiplied pairwise for closure
	int c_range[domain_size+1];  // C inverse of c_map
	for (int idx = 1; idx <= domain_size; ++idx) {
		c_oldS[idx] = 0;
		c_newS[idx] = 0;
		c_range[idx] = 0;
	}

	Obj in_domain = ELM_PLIST(f, 2);
	for (int idx = 1; idx <= LEN_PLIST(in_domain); ++idx) {
		c_newS[(int)INT_INTOBJ(ELM_PLIST(in_domain, idx))] = 1;
	}

	Obj in_range = ELM_PLIST(f, 3);
	for (int idx = 1; idx <= LEN_PLIST(in_range); ++idx) {
		c_range[(int)INT_INTOBJ(ELM_PLIST(in_range, idx))] = 1;
	}

	int has_new = 1;
	int c_pairs[domain_size+1][domain_size+1];  // c_pairs[x][y] = 1 is to mark (x, y) as an ordered pair.

	while (has_new > 0)
	{
		has_new = 0;
		int c_newNow[domain_size+1];  // c_newNow[z] == 1 means z is in the set c_newNow
		for (int idx = 1; idx <= domain_size; ++idx) {
			for (int jdx = 1; jdx <= domain_size; ++jdx)
				c_pairs[idx][jdx] = 0;
			c_newNow[idx] = 0;
		}

		for (int x = 1; x <= domain_size; ++x) {
			if (c_oldS[x] > 0) {
				for (int y = 1; y <= domain_size; ++y)
					if (c_newS[y] > 0) {
						c_pairs[x][y] = 1;
						c_pairs[y][x] = 1;
					}
			}
		}
		for (int x = 1; x <= domain_size; ++x) {
			if (c_newS[x] > 0){
				for (int y = 1; y <= domain_size; ++y) {
					if (c_newS[y] > 0)
						c_pairs[x][y] = 1;
				}
			}
		}

		for (int idx = 1; idx <= domain_size; ++idx) {
			for (int jdx = 1; jdx <= domain_size; ++jdx) {
				if (c_pairs[idx][jdx] > 0) {
					int x = idx;
					int y = jdx;
					int z = c_L[x][y];
					int fz = c_M[c_map[x]][c_map[y]];
					if (c_map[z] == 0) {  // found new element that has not been mapped
						c_map[z] = fz;
						SET_ELM_PLIST(in_map, z, INTOBJ_INT(fz));
						C_ADD_LIST_FPL(in_domain, INTOBJ_INT(z));
						if (c_range[fz] == 0) {
							c_range[fz] = 1;
							C_ADD_LIST_FPL(in_range, INTOBJ_INT(fz));
						}
						c_newNow[z] = 1;
						has_new = 1;
					}
					else if (c_map[z] != fz) // check homomorphism
						return Fail;
				}
			}
		}
		for (int idx = 1; idx <= domain_size; ++idx) {
			if (c_newS[idx] > 0)
				c_oldS[idx] = c_newS[idx];
			c_newS[idx] = c_newNow[idx];
		}
	}

	return f;
}


static StructGVarFunc GVarFuncs [] = {
{ "MAGMAS_ExtendHomomorphismByClosingSource_C", 3, "f, L, M", MAGMAS_ExtendHomomorphismByClosingSource_C, "src/iso.c:MAGMAS_ExtendHomomorphismByClosingSource_C" },
{ "MAGMAS_AutomorphismsFixingSet_C", 5, "S, MT_Q, x, GenQ, DisQ", MAGMAS_AutomorphismsFixingSet_C, "src/iso.c:MAGMAS_AutomorphismsFixingSet_C" },
{ "MAGMAS_Invariants_C", 2, "MT_Q, StructType", MAGMAS_Invariants_C, "src/iso.c:MAGMAS_Invariants_C" },
};



/**************************************************************************

*F  InitKernel( <module> )  . . . . . . . . initialise kernel data structures
*/
static Int InitKernel (
    StructInitInfo *    module )
{

    /* init filters and functions                                          */
    InitHdlrFuncsFromTable( GVarFuncs );

    /* return success                                                      */
    return 0;
}


/****************************************************************************
**
*F  InitLibrary( <module> ) . . . . . . .  initialise library data structures
*/
static Int InitLibrary (
    StructInitInfo *    module )
{
  /*    UInt            gvar;
	Obj             tmp; */

    /* init filters and functions                                          */
    /* printf("Init El..Small\n");fflush(stdout); */
    InitGVarFuncsFromTable( GVarFuncs );

    /* return success                                                      */
    return 0;
}


/****************************************************************************
**
*F  InitInfopl()  . . . . . . . . . . . . . . . . . table of init functions
*/
/* <name> returns the description of this module */
static StructInitInfo module = {
#ifdef EDIVSTATIC
    .type = MODULE_STATIC,
#else
    .type = MODULE_DYNAMIC,
#endif
    .name = "hello",
    .initKernel = InitKernel,
    .initLibrary = InitLibrary,
};

#ifndef EDIVSTATIC
StructInitInfo * Init__Dynamic ( void )
{
 return &module;
}
#endif

StructInitInfo * Init__ediv ( void )
{
  return &module;
}






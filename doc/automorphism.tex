\documentclass[12pt,reqno]{amsart}
\usepackage{fullpage}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{times}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage[utf8]{inputenc}
\usepackage[portuges,english]{babel} %para ter os nomes das seccoes, etc em portugues
\usepackage{hhline}
\usepackage{url}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{makecell}
\vfuzz=2pt

\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}[thm]{Corollary}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
{ \theoremstyle{remark}\newtheorem*{remark}{Remark} }

\newcommand{\class}{Magma Automorphism}

\begin{document}

\title{Fast Algorithm To Compute Automorphisms and Isomorphisms for Finite Magmas}
% or if you want, simply \title{Title of the article}
%\author{João Araújo}
%\email{jjrsga@gmail.com}
%\author{Choiwah Chow}
%\email{choiwah.chow@gmail.com}
\maketitle

\begin{abstract}
While there are efficient algorithms to compute the automorphisms of groups and quasigroups, there are no similar facilities available for the more general algebraic structure, magmas. In this paper, we propose to extend the Discriminator concept in GAP's Loops package for quasigroups to handle magmas. Based on this extension, we devise simple and yet efficient algorithms to find the automorphism groups of magmas, and to find, if exists, the isomorphism between any given pair of magmas. We then extended this algorithm to finding the automorphism group of algebra of type ($2^m, 1^n$) where $m>0 and n\ge 0$.
\end{abstract}

\section{Introduction}
\label{intro section}

Fast algorithms exist to find the automorphisms of a group.  For example, GAP \cite{GAP4}, a system for computational discrete algebra, uses many special properties of the group to implement the function {\em AutomorphismGroup} to efficiently find the automorphism group of a given group. However, this specialized method, while extremely efficient, works only on groups and not on any other more general algebraic structures, such as quasigroups, semigroups and magmas. Likewise, the Loops package \cite{LOOPS3_4_1} in GAP provides another version of the function {\em AutomorphismGroup} to compute all the automorphisms of a given quasigroup, but the function does not work for any other algebraic structures either.  It certainly also works for groups, which are also quasigroups, albeit slower than the specialized version for groups. Nonetheless, it is a very fast and elegant algorithm. In fact, the Loops package provides one of the best known algorithms for finding automorphisms of quasigroups, and for finding isomorphisms between quasigroups. 

To date, there are no known implementations of functions for finding the automorphisms of magmas in GAP.  To fill the void, we extend the {\em Discriminator} concept implemented in the Loops package to compute the {\em Invariant Vector} for magmas.  With this invariant vector, we can apply similar algorithms used in the Loops package to efficiently find the automorphisms of magmas, and to find the isomorphisms between any pairs of magmas.  We implemented these algorithms as the {\em AutomorphismGroup} and the {\em IsomorphismMagma} functions in the new GAP package {\em Magmaut} \cite{MAGMAUT_0_1_0}.

\section{The Algorithms}
\label{algorithm}

One way to find an isomorphism between two not necessarily distinct magmas is to try one-by-one all possible mappings between their elements and check that the resulting mapping constitutes an isomorphism. This is certainly inefficient when the domain size is large. Fortunately, there are many ways to cut down the search space:
\begin{enumerate}
	\item Limit the search to bijections, that is, different elements in one magma will only be mapped to different elements in the other magma.
	\item Limit the search to mapping of elements with the same characteristics, as shown in their invariant vectors, from one magma to another. This will be explained in section \ref{invariant}.
	\item Limit the search to a generating set. This will be discussed in section \ref{generating set}.
\end{enumerate}
 
\subsection{Invariant Vector}
\label{invariant}
In a homomorphism, elements in a domain only be mapped to compatible elements with the same properties in the range. For example, if $e, f$ are elements in a magma $M$, and $h:M\to M$ is mapping such that $h(e)=f$. Suppose further that $e$ is an idempotent and $f$ is not, then we can immediately conclude that $h$ is not a homomorphism because $h(e)h(e)=ff\neq{f}=h(e)$, violating the definition of homomorphism.  In this case, we would consider idempotent an invariant property of an element, because this property has to be preserved in homomorphisms.  This is the basis of the {\em Discriminator} in the Loops package, which implemented nine such invariants for the domain elements of quasigroups. However, most of these invariants cannot be carried over directly to magmas. We identified those that can be extended to magmas, and added some new ones, for a total of eleven, to the Magmaut package.  For each element, $p$, in a magma, we calculate its invariant vector:
\begin{enumerate}
	\item Smallest $k$ such that $p^k = p^n$ such that $n > k > 1$.
	\item Number of domain elements for which $p$ is a left identity.
	\item Number of domain elements for which $p$ is a right identity.
	\item For each element x, number of elements y such that x = (xy)x
	\item Number of distinct elements on a row of the multiplication table
	\item Number of distinct elements on a column of the multiplication table
	\item 1 if $p$ is an idempotent, 0 otherwise.
	\item Number of idempotents in a column of the multiplication table
	\item Number of idempotents in a row of the multiplication table	
	\item 1 if the equality $p*(p*p) = (p*p)*p$ holds, 0 otherwise.
	\item Number of domain elements that $p$ commutes with.
	\item Number of domain elements $s$ for which $(s*s)*p=p*(s*s)$.
	\item Number of domain elements $s$ such that $s^2=p$.
	\item Number of domain elements $s$ satisfying $p*(p*s) = (p*p)*s$
	\item Number of products $xy = yx = p$
	\item Number of t such that two idempotents e,f in M and $p=et=tf$
	\item Number of t for which there exist two elements x,y in M such that $p=xy$ and $t=yx$
\end{enumerate}


\subsection{Generating Set}
\label{generating set}
Recall a generating set of a magma $M$ is the subset $S$ of $M$ such that every element in $M$ can be expressed as a finite product of the elements in $S$. We can cut down the size of the search tree by focusing only on mappings of elements in the generating set because mappings of elements outside the generating set depend entirely on those of the generating set. To wit, let $m$ be an element in $M\setminus{S}$ such that $m=s_1{s_2}\dots{s_n}$ where $s_1,s_2,\dots{s_n}\in S$, and $h:M\to N$ be a homomorphism, then by definition, $h(m)=h(s_1{s_2}\dots{s_n})=h(s_1)h(s_2)\dots{h}({s_n})$. Thus, the mapping of elements outside $S$ are completely fixed by the mappings of those in $S$.

Recall that a magma may have multiple generating sets.  Some may be more suitable for use in finding automorphisms and isomorphisms than others:
\begin{itemize}
	\item A smaller generating set is preferred because it would require fewer trials in constructing an isomorphism.
	\item Elements with unique invariant vectors are preferred because elements can only be mapped to elements with the same invariant vector in a homomorphism. If elements with the same invariant vector are blocked together, then those from the smallest blocks would be preferred.  Thus, generating set with more elements from the smaller blocks are preferred.
\end{itemize}

\begin{samepage}
The algorithm that satisfies both the constraints above in generating an efficient generating set is depicted in Algorithm \ref{efficentGSet}.

\begin{algorithm}
\caption{Constructing Efficient Generating Set}
\begin{algorithmic}
	\STATE $submagma \leftarrow [\ ]$
	\STATE $generator \leftarrow [\ ]$
	\STATE $candidates \leftarrow $ magma elements sorted by ascending block size
	\WHILE{$submagma \neq magma$}
		\STATE $generator \leftarrow generator\ \cup$ the element that increase the size of $submagma$ most, and if 2 elements increase the size by the same amount, take the one from the smaller block.
		\STATE $submagma \leftarrow $ submagma generated by $generator$
		\STATE $candidates \leftarrow candidates$ with elements in submagma removed
	\ENDWHILE
\end{algorithmic}
\label{efficentGSet}
\end{algorithm}
\end{samepage}

\subsection{The Automorphism Algorithm}
The algorithm for finding the automorphism group of a given magma, as outlined before, is to start with computing invariant vectors of its elements, from which we obtain an efficient generating set. Recall that we only need to map the generating set, and each element in the generating set can only map to elements having the same invariant vector, that is, elements in the same block.  We can easily extend the partial maps to a full map by generating all the elements of the magma from the generating set, and check that isomorphism conditions are satisfied. Finally, we construct a group (the automorphism group) out of the isomorphisms found.

The algorithm for generating isomorphism between two magmas is quite similar in principles to the automorphism algorithm, and its description will be omitted here.

We tested the algorithm on groups, loops, and quasigroups, and confirmed the correctness of the results against the Loops package. For magmas of orders 2 and 3, and semigroups of orders 6 and 7, we verified them against the Mace4 program \cite{MACE}.

\subsection{Algebras of Type ($2^m, 1^n$)}
\label{subsection:algebra_type_2}
Given an algebra of type ($2^m, 1^n$), that is, an algebra comprises of a finite number of binary operations and unary operations, its automorphism group can be computed by taking the intersection of all the automorphism groups of the binary operations that also commute, function composition-wise, with all the unary operations.

Recall that in our automorphism algorithm, the automorphism group is constructed with the generating set of automorphisms. We can therefore filter out those automorphisms in the generating set that do not commute with all the unary operations before we construct the automorphism group. As a result, the automorphism groups so constructed will be smaller and their intersection is easier to compute.

The invariant vector of each domain element with respect to each binary operation can be pooled together to increase their discriminating powers. This not only increases the speed by cutting down the search space for automorphic mappings, but also cuts down the size of the generating sets of the automorphism group, resulting in smaller automorphism groups for which the intersection is easier to compute.

Another useful observation is that the intersection of the trivial group and any group is the trivial group. Therefore, if any of the automorphism group generated in the process is a trivial group, then the search can be terminated with the trivial group declared the automorphism group for the algebra.


\section{Performance}
We ran the Magmaut package to generate automorphism groups on quasigroups, loops and groups to compare the timings against the Loops package.  The hardware used was an Intel 10th generation 4-core i7 machine with 8GB of RAM.  All experiments were run 7 times, with the lowest and highest times discarded. The averages of the remaining 5 are reported in Table \ref{magmautvsloops}. 

We see that the speeds from both packages are quite close.

\begin{table}[ht]
	\caption{Performance Comparison between Loops and Magmaut on Automorphism Group Generation.}\label{magmautvsloops}
	\renewcommand\arraystretch{1.5}
	\noindent\[
	\begin{tabular}{|c|c|c|}
	\hline
	Algebraic Structure&Loops Package(sec)&Magmaut Package(sec)\\
	\hhline{|=|=|=|}
	Quasigroups, order 5 & 0.588 & 0.562\\
	\hline
	Quasigroups, order 6 & 520 & 541\\
	\hhline{|=|=|=|}
	Loops, order 6 & 0.101 & 0.086\\
	\hline
	Loops, order 7 & 14.623 & 13.945\\
	\hhline{|=|=|=|}
	Groups, order 32 & 0.854 & 0.935\\
	\hline
	Groups, order 64 & 39.98 & 41.05\\
	\hline
	Groups, order 128 & 12,031 & 12,160\\
	\hline
	\end{tabular}
	\]
\end{table}

We have also run experiments on generating automorphism groups for magmas and semigroups.  Results are displayed on Table \ref{magmasemi}. For these experiments, we don't have results from other GAP packages to compare with.
\begin{table}[ht]
	\caption{Performance of Magmaut on Automorphism Group Generation for Magmas and Semigroups.}\label{magmasemi}
	\renewcommand\arraystretch{1.5}
	\noindent\[
	\begin{tabular}{|c|c|}
	\hline
	Algebraic Structure&Time(sec)\\
	\hhline{|=|=|}
	Magmas, order 2 & 0.016\\
	\hline
	Magmas, order 3 & 1.253\\
	\hhline{|=|=|}
	Semigroups, order 6 & 2.316\\
	\hline
	Semigroups, order 7 & 45.947\\
	\hline
	\end{tabular}
	\]
\end{table}

The Algebra\_AutomorphismGroup, with the performance enhancements described in Section~\ref{subsection:algebra_type_2} runs substantially faster than the simple intersection of automorphism groups, especially when the automorphism group is the trivial group. Sample results are shown in Table~\ref{table:algebra_Auto}.
\begin{table}[ht]
	\caption{Performance Comparison between Algebra\_AutomorphismGroup and Intersection of Automorphism Groups.}\label{table:algebra_Auto}
	\renewcommand\arraystretch{1.5}
	\noindent\[
	\begin{tabular}{|c|r|r|c|}
		\hline
		Algebra of type ($2^m$)&\makecell{\\Algebra\\\_AutomorphismGroup \\ (sec)}&\makecell{Intersection of \\Automorphism \\Groups\\(sec)}&\makecell{\\Is Trival Group \\the Automorphism \\Group?}\\
		\hhline{|=|=|=|=|}
		\makecell{All 2,328 non-isomorphic \\groups of order 128} & 153.8 & 201.5&No\\
		\hline
		\makecell{All 67 non-isomorphic \\groups of order 243} & 10.3 & 56.3&No\\
		\hline
		\makecell{All 15 non-isomorphic \\groups of order 625}& 11.3 & 122.9&No\\
		\hline
		\makecell{All 15 non-isomorphic \\groups of order 999} & 2.1 & 168.9&Yes\\
		\hline
	\end{tabular}
	\]
\end{table}

\section{Conclusion}

We have extended an algorithm that works for quasigroups to handle a more general class of algebra, the magma. Since magma is the most basic kind of algebraic structure, our algorithm can be applied to a wide range of sub-classes of magma such as semigroup and monoid for which no specialized methods for automorphism and isomorphisms are available. This algorithm is both efficient and versatile, as we have seen that it can easily be extended to handle a more complex algebraic structure,  algebra of type ($2^m, 1^n$). We have also applied the algorithm to check whether two magmas are isomorphic, and if so, returns the isomorphism.  This algorithm is implemented in the AutomorphismGroup and the IsomorphismMagma functions in the GAP package Magmaut, which is available to the general public.


\begin{thebibliography}{99}
	\bibitem[GAP]{GAP4}
	The GAP~Group, \emph{GAP -- Groups, Algorithms, and Programming, Version 4.11.0}; 
	2020, \url{https://www.gap-system.org}.
	
	\bibitem[LOOPS]{LOOPS3_4_1}
	Gábor Nagy \& Petr Vojtěchovský, \emph{The LOOPS Package - a GAP package, Version 3.4.1}; 
	2018, \url{https://gap-packages.github.io/loops/}.
	
	\bibitem[MACE4]{MACE}
	W. McCune, \emph{Mace4, Version 2009-02A}; 2009
	\url{https://www.cs.unm.edu/~mccune/prover9/manual/2009-02A/mace4.html}.
	
	\bibitem[MAGMAUT]{MAGMAUT_0_1_0}
	João Araújo \& Choiwah Chow, \emph{The MAGMAT Package - a GAP package, Version 0.1.0}; 
	2020, \url{https://gap-packages.github.io/magmaut/}.

\end{thebibliography}



\end{document}
